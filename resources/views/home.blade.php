@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header">Courses Offered</div>

                <div class="card-body">

                        @if($courses->isEmpty())
                            <p>
                                There is no course Yet !! <a href="{{ url('create') }}">Create one</a> now.
                            </p>
                        @else
                        <ul class="list-group">
                            @foreach($courses as $course)
                                <li class="list-group-item">
                                    <a href="{{ url('edit', [$course->slug]) }}">
                                        {{ $course->course_name }}
                                    </a>
                                    <!--span class="pull-right">{{ $course->updated_at->diffForHumans() }}</span-->
                                </li>
                            @endforeach
                        </ul>
                        @endif

                        

                </div>
                <div class="card-body">{{ $courses->links() }}</div>


            </div>
        </div>
    </div>

        
</div>
@endsection
