<?php

use Illuminate\Database\Seeder;
use App\Course;

class CourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * https://packagist.org/packages/mbezhanov/faker-provider-collection
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $faker->addProvider(new \Bezhanov\Faker\Provider\Educator($faker));
 
        // Create 20 product records
        for ($i = 0; $i < 20; $i++) {
            Course::create([
                'course_name' => $faker->course,
            ]);
        }
    }
}
